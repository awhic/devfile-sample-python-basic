from unittest import TestCase

from calc import average_rate_of_depletion_prediction


class Test(TestCase):

    def test_empty_data(self):
        """Test with empty data list"""
        data = []
        self.assertIsNone(average_rate_of_depletion_prediction(data))

    def test_invalid_data(self):
        """Test with data missing 'value' or 'time' keys"""
        data = [{'time': 1}, {'value': 2}]
        self.assertIsNone(average_rate_of_depletion_prediction(data))

    def test_no_depletion(self):
        """Test with data where no depletion occurs"""
        data = [{'value': 100, 'time': 1}, {'value': 100, 'time': 2}]
        self.assertIsNone(average_rate_of_depletion_prediction(data))

    def test_normal_depletion(self):
        """Test with normal depletion data"""
        data = [{'value': 100, 'time': 1}, {'value': 50, 'time': 2}, {'value': 0, 'time': 3}]
        self.assertEqual(average_rate_of_depletion_prediction(data), 3)

    def test_negative_depletion_rate(self):
        """Test with data that has a negative depletion rate"""
        data = [{'value': 50, 'time': 1}, {'value': 100, 'time': 2}]
        self.assertIsNone(average_rate_of_depletion_prediction(data))

    def test_zero_total_time(self):
        """Test with data where total time is zero"""
        data = [{'value': 100, 'time': 1}, {'value': 50, 'time': 1}]
        self.assertIsNone(average_rate_of_depletion_prediction(data))
