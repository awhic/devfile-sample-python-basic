from flask import Flask, request
import os

from calc import average_rate_of_depletion_prediction

app = Flask(__name__)


@app.route('/')
def hello():
    return "Hello World!"


@app.route('/api/v1/insights', methods=['POST'])
def ai_insights():
    data = request.get_json()

    if isinstance(data, list) and len(data) > 0:
        predicted_zero_time = average_rate_of_depletion_prediction(data)
        return str(predicted_zero_time) if predicted_zero_time is not None else '0'
    else:
        return '0'


if __name__ == '__main__':
    port = os.environ.get('FLASK_PORT') or 8080
    port = int(port)

    app.run(port=port, host='0.0.0.0')
