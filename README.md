# Easy Fleet Flask Service

## Description

A companion Flask API responsible for serving AI insights for the EasyFleet Service by request.

## Table of Contents

- [Local Installation](#local-installation)
    - [Quickstart](#quickstart)
- [Openshift Deployment](#openshift-deployment)
    - [Instructions](#instructions)
- [Usage](#usage)
- [API](#api)
- [Additional Resources](#additional-resources)
- [Contact](#contact)
- [License](#license)

## Local Installation

### Prerequisites

Before you begin, ensure you have the following installed:

- Python 3.9 or later ([download the latest](https://www.python.org/downloads/))

### Quickstart

1. Open a terminal in the project root and run the following commands:
   - `py -3 -m venv .venv`
   - `.venv\Scripts\activate`
   - `pip install Flask`
   - `flask --app app run`
2. The Flask API will be running on http://127.0.0.1:5000.
3. (Optionally) test the API by hitting http://127.0.0.1:5000/.
    - Expected output: `Hello World!`.

## OpenShift Deployment

### Instructions

1. Navigate to "+Add" > "Import from Git":
    - Insert: https://gitlab.com/awhic/devfile-sample-python-basic

![img.png](assets/import-git.png)

2. Change the import strategy to "Dockerfile" and change the directory to `docker/Dockerfile`:

![img.png](assets/import-strategy.png)

3. In the "General" section:
    - Application - Place in the same Application Group as the Main Application, or create a new one if not applicable.
    - Name - `easy-fleet-flask-service`
    - Resource Type - **Serverless** Deployment
4. Set Target port to `8081`.
5. Check the box for "Create Route".
6. Click "Create".

## Usage

This Flask API is designed to take in large sets of real-time data and serve AI insights to the Resource Server via REST.
The API currently utilizes low-level mathematical operations to make performance "predictions", mocking the behavior of
a trained model that might utilize a form of non-linear regression as a more accurate method of prediction.

## API

### Fetch AI Insights

- **Endpoint**: `POST /api/v1/insights`
- **Description**: Retrieve a predicted timestamp for the next needed charge.
- **Headers**:
    - `Content-Type: application/json`
- **Body**:

```json
{
  "value": "200",
  "time": "1699852468"
}
```
>Note: The value is the battery status in kW and the time is a Unix epoch timestamp.

## Additional Resources

- [Python](https://www.python.org/)
- [Flask](https://flask.palletsprojects.com/en/3.0.x/)
- [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

## Contact

For any inquiries, please feel free to reach out to me at [awhixk@gmail.com](mailto:awhixk@gmail.com).

If you need to speak to me, I can be reached at 502-322-5742.

## License

This project is licensed under the Apache License 2.0 - see [LICENSE.md](LICENSE.md).
